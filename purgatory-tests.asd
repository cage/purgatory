;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(defsystem :purgatory-tests
  :description "Test suite for 9p-client."
  :author      "cage"
  :license     "LLGPL"
  :version     "0.0.1"
  :serial      t
  :pathname    "tests"
  :depends-on (:alexandria
               :cl-ppcre
               :cl+ssl
               :clunit2
               :usocket
               :babel
               :uiop
               :purgatory)
  :components ((:file "package")
               (:file "all-tests")
               (:file "protocol-tests")))
