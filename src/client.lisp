;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :purgatory)

(a:define-constant +byte-type+            '(unsigned-byte 8) :test #'equalp)

(a:define-constant +version+               "9P2000"          :test #'string=)

(a:define-constant +message-length-size+          4          :test #'=)

(a:define-constant +message-type-size+            1          :test #'=)

(a:define-constant +message-tag-size+             2          :test #'=)

(a:define-constant +message-string-length-size+   2          :test #'=)

(a:define-constant +nofid+                        #xffffffff :test #'=)

(a:define-constant +create-for-read+       #x0               :test #'=)

(a:define-constant +create-for-write+      #x1               :test #'=)

(a:define-constant +create-for-read-write+ #x2               :test #'=)

(a:define-constant +create-for-exec+       #x3               :test #'=)

(a:define-constant +create-dir+            #x80000000        :test #'=)

(a:define-constant +open-truncate+         #x10              :test #'=)

(a:define-constant +open-remove-on-clunk+  #x40              :test #'=)

(a:define-constant +stat-type-dir+          #x80       :test #'=
  :documentation "mode bit for directories")

(a:define-constant +stat-type-append+       #x40       :test #'=
  :documentation "mode bit for append only files")

(a:define-constant +stat-type-excl+         #x20       :test #'=
  :documentation "mode bit for exclusive use files")

(a:define-constant +stat-type-mount+        #x10       :test #'=
  :documentation "mode bit for mounted channel")

(a:define-constant +stat-type-auth+         #x08       :test #'=
  :documentation "mode bit for authentication file")

(a:define-constant +stat-type-tmp+          #x04       :test #'=
  :documentation "mode bit for non-backed-up files")

(a:define-constant +stat-type-symlink+      #x02       :test #'=
  :documentation "mode bit for non-backed-up files")

(a:define-constant +stat-type-file+         #x00       :test #'=
  :documentation "mode bit for non-backed-up files")

(a:define-constant +file-types+ (list (cons +stat-type-dir+     :directory)
                                      (cons +stat-type-append+  :append-only)
                                      (cons +stat-type-excl+    :executable)
                                      (cons +stat-type-mount+   :mount)
                                      (cons +stat-type-auth+    :auth )
                                      (cons +stat-type-tmp+     :tmp)
                                      (cons +stat-type-symlink+ :symlink)
                                      (cons +stat-type-file+    :file)
                                      (cons #xff                :do-not-care))
  :test #'equalp)

(a:define-constant +9p-write-header-fixed-size+ (+ +message-length-size+
                                                 +message-type-size+
                                                 +message-tag-size+
                                                 4 ; fid size
                                                 8 ; offset
                                                 4) ;count
  :test #'=)

(a:define-constant +9p-read-header-fixed-size+ (+ +message-length-size+
                                                +message-type-size+
                                                +message-tag-size+
                                                4 ; fid size
                                                8 ; offset
                                                4) ;count
  :test #'=)

(defun file-type-number->symbol (key)
  (cdr (assoc key +file-types+)))

(defun symbol->file-type-number (key)
  (let ((cons-cell (find-if (lambda (a) (eq (cdr a) key)) +file-types+)))
    (car cons-cell)))

;; modes

(a:define-constant +stat-type-read+         #x4              :test #'=
  :documentation "mode bit for read permission")

(a:define-constant +stat-type-write+        #x2              :test #'=
  :documentation "mode bit for write permission")

(a:define-constant +stat-type-exec+         #x1              :test #'=
  :documentation "mode bit for execute permission")

(a:define-constant +standard-socket-port+   564              :test #'=)

(a:define-constant +nwname-clone+             0              :test #'=)

(defparameter *buffer-size*  (* 4 1024 1024))

(defparameter *tag* 8)

(defparameter *fid* #x00000001)

(defparameter *messages-sent* '())

;; internal procedures

(defun tags-exists-p-clsr (tag-looking-for)
  (lambda (a) (octects= tag-looking-for (car a))))

(defun fire-response (tag message-type data)
  (let ((found (find-if (tags-exists-p-clsr tag) *messages-sent*)))
    (if found
        (let ((fn (cdr found)))
          (setf *messages-sent* (remove-if (tags-exists-p-clsr tag) *messages-sent*))
          (funcall fn message-type data))
        (warn (format nil "received unknown response message tag ~a" tag)))))

(defun ignore-response (tag)
  (let ((found (find-if (tags-exists-p-clsr tag) *messages-sent*)))
    (if found
        (setf *messages-sent* (remove-if (tags-exists-p-clsr tag) *messages-sent*))
        (warn (format nil "received unknown response message tag ~a" tag)))))

(defun append-tag-callback (tag function)
  (setf *messages-sent* (push (cons tag function) *messages-sent*)))

(defun read-all-pending-messages (stream)
  "The responses from  the server are not  processed immediately, they
are collected until this function is called."
  (declare (optimize (debug 0) (speed 3)))
  (when *messages-sent*
    (multiple-value-bind (message-type rtag data)
        (restart-case
            (read-message stream)
          (ignore-error (e)
            (values (message-type e)
                    (tag          e)
                    nil)))
      (if data
          (fire-response rtag message-type data)
          (ignore-response rtag))
      (read-all-pending-messages stream))))

(defun next-tag ()
  (prog1
      (make-octects *tag* 2)
    (setf *tag*
          (max 8
               (rem (1+ *tag*)
                    (expt 2 (* 8 +message-tag-size+)))))))

(defun next-fid ()
  "Generate a fresh fid"
  (prog1
      (int32->bytes *fid*)
    (incf *fid*)))

(defun bytes->int (bytes)
  "Convenience function to convert a vector of bytes to a number using
the correct bytes order."
  (let ((res #x0000000000000000)
        (ct  0))
    (map nil
         (lambda (a)
           (setf res (boole boole-ior
                            (ash a ct)
                            res))
           (incf ct 8))
         bytes)
    res))

(defmacro gen-intn->bytes (bits)
  (let ((function-name (alexandria:format-symbol t "~:@(int~a->bytes~)" bits)))
    `(defun ,function-name (val &optional (count 0) (res '()))
       (if (>= count ,(/ bits 8))
           (reverse res) ; little endian
           (,function-name (ash val -8)
                           (1+ count)
                           (push (boole boole-and val #x00ff)
                                 res))))))

(gen-intn->bytes  8)

(gen-intn->bytes  16)

(gen-intn->bytes  32)

(gen-intn->bytes  64)

(gen-intn->bytes 512)

(gen-intn->bytes 416)

(defun big-endian->little-endian (bytes)
  (reverse bytes))

(defun vcat (a b)
  (concatenate 'vector a b))

(defclass octects ()
  ((value
    :initform 0
    :initarg :value
    :accessor value)
   (size
    :initform 0
    :initarg :size
    :accessor size)))

(defgeneric octects= (a b))

(defgeneric encode (object))

(defgeneric decode (object))

(defmethod encode ((object octects))
  "Encode an octect to be trasmitted on the stream"
  (with-accessors ((value value)
                   (size size)) object
    (let ((bytes (ecase size
                   (1  (int8->bytes  value))
                   (2  (int16->bytes  value))
                   (4  (int32->bytes  value))
                   (8  (int64->bytes  value))
                   (13 (int416->bytes value))
                   (32 (int512->bytes value))))
          (res   (make-array size :element-type +byte-type+)))
      (loop for i from 0 below size do
        (setf (elt res i) (elt bytes i)))
      res)))

(defmethod octects= ((a octects) b)
  (= (value a) b))

(defmethod octects= ((a number) (b octects))
  (octects= b a))

(defmethod octects= ((a number) (b number))
  (= b a))

(defun add-size (msg)
  (let ((length (int32->bytes (+ +message-length-size+ (length msg)))))
    (vcat length msg)))

(defun close-ssl-socket (socket)
  (usocket:socket-close socket))

(defun close-client (socket)
  (close-ssl-socket socket))

(defun send-message (stream message)
  (write-sequence message stream)
  (finish-output stream))

(defun encode-string (string)
  (let* ((bytes (babel:string-to-octets string))
         (size  (int16->bytes (length bytes))))
    (vcat size bytes)))

(defmethod encode ((object string))
  (encode-string object))

(defmethod encode ((object list))
  (let ((buffer (make-message-buffer (length object))))
    (loop for i from 0 below (length object) do
      (setf (elt buffer i) (elt object i)))
    buffer))

(defmethod encode (object)
  object)

(defmethod decode-string (data)
  (let ((size (bytes->int (subseq data 0 +message-string-length-size+))))
    (babel:octets-to-string (subseq data
                                    +message-string-length-size+
                                    (+ +message-string-length-size+ size))
                            :errorp nil)))

(defun compose-message (message-type tag &rest params)
  (let ((actual-params (reduce #'vcat (mapcar #'encode params))))
    (add-size (reduce #'vcat (list (encode message-type) (encode tag) actual-params)))))

(defun displace-response (response)
  (let ((message-type (subseq response 0 +message-type-size+))
        (message-tag  (subseq response
                              +message-type-size+
                              (+ +message-type-size+
                                 +message-tag-size+)))
        (data         (subseq response
                              (+ +message-type-size+
                                 +message-tag-size+))))
    (values (bytes->int message-type)
            (bytes->int message-tag)
            data)))

(defun make-message-buffer (size)
  (make-array size :element-type +byte-type+))

(defun error-response-p (response)
  (multiple-value-bind (message-type x y)
      (displace-response response)
    (declare (ignore x y))
    (= message-type *rerror*)))

(defun read-message (stream)
  (let ((message-length-buffer (make-message-buffer +message-length-size+)))
    (read-sequence message-length-buffer stream)
    (let* ((message-length (bytes->int message-length-buffer)))
      (if (<= message-length +message-length-size+)
          (error (text:strcat "Unrecoverable error: invalid response from server. "
                              "This could be either a client or a server bug. "
                              "Also, if TLS, is used this error can be signalled if "
                              "TLS certificate was not accepted by the server"))
          (let ((buffer (make-message-buffer (- message-length +message-length-size+))))
            (read-sequence buffer stream)
            (multiple-value-bind (message-type tag data)
                (displace-response buffer)
              (if (error-response-p buffer)
                  (error '9p-error
                         :message-type message-type
                         :tag          tag
                         :error-value (decode-string data))
                  (values message-type tag data))))))))

(defun make-octects (number size)
  (make-instance 'octects :value number :size size))

;; protocol implementation

(defun send-version (stream tag)
  (let ((message (compose-message (make-octects *tversion* 1)
                                  tag
                                  (make-octects *buffer-size* 4)
                                  +version+)))
    (send-message stream message)
    (multiple-value-bind (message-type rtag data)
        (read-message stream)
      (assert (= message-type *rversion*))
      (if (octects= rtag tag)
          (let ((message-size     (bytes->int    (subseq data 0 4)))
                (protocol-version (decode-string (subseq data 4))))
            (if (string= protocol-version +version+)
                (progn
                  (setf *buffer-size* message-size)
                  (values message-size protocol-version))
                (error '9p-error
                       :message-type message-type
                       :tag          tag
                       :error-value  (format nil
                                             "Version mismatch: ~s instead of ~s"
                                             protocol-version
                                             +version+))))
          (error '9p-initialization-error :tag tag :rtag rtag)))))

(defmacro with-new-tag ((tag) &body body)
  `(let ((,tag (next-tag)))
     ,@body))

(defmacro with-new-fid ((fid) &body body)
  "Useful convenience macro to generate a new fid"
  `(let ((,fid (next-fid)))
     ,@body))

(defun initialize-session (stream)
  (with-new-tag (tag)
    (multiple-value-bind (buffer-size protocol-version)
        (send-version stream tag)
      (values protocol-version buffer-size))))

(defun decode-quid (data)
  (let ((file-type    (a:first-elt data))
        (file-version (subseq data 1 4))
        (file-path    (subseq data 1 5)))
    (values file-type
            (bytes->int file-version)
            (bytes->int file-path))))

(defun dummy-callback (message-type data)
  "A callback that does nothing"
  (declare (ignore message-type data)))

(defun dump-callback (message-type data)
  "A callback that  print the data on the standard  output, useful for
debugging"
  (format t "reply mtype  ~a ~a~%" message-type data))

(defgeneric 9p-attach (stream root &key username callback))

(defmethod 9p-attach (stream (root string)
                      &key
                        (username "nobody")
                        (callback #'dummy-callback)
                        (afid     (make-octects +nofid+ 4)))
  (with-new-tag (tag)
    (with-new-fid (root-fid)
      (let* ((message (compose-message (make-octects *tattach* 1)
                                       tag
                                       root-fid
                                       afid
                                       username
                                       root)))
        (append-tag-callback tag callback)
        (send-message stream message)
        root-fid))))

(defun make-octects-permissions (mode)
  (make-octects mode 4))

(defun 9p-create (stream parent-dir-fid path
                  &key
                    (callback    #'dummy-callback)
                    (permissions #o640)
                    (mode        +create-for-read-write+))
  "Note: path is relative to root, see attach,
   Also note that successfully creating a file will open it."
  (with-new-tag (tag)
    (let* ((message (compose-message (make-octects *tcreate* 1)
                                     tag
                                     parent-dir-fid
                                     path
                                     (make-octects-permissions permissions)
                                     (make-octects mode 1))))
      (append-tag-callback tag callback)
      (send-message stream message))))

(defun 9p-open (stream fid
                &key
                  (callback #'dummy-callback)
                  (mode     +create-for-read+))
  "Note: before opening you have to 'walk' the file to get the corresponding fid."
  (with-new-tag (tag)
    (let* ((message (compose-message (make-octects *topen* 1)
                                     tag
                                     fid
                                     (make-octects mode 1))))
      (append-tag-callback tag callback)
      (send-message stream message))))

(defgeneric 9p-write (stream fid offset data &key callback))

(defmethod 9p-write (stream fid offset (data vector)
                     &key
                       (callback #'dummy-callback))
  (when (not (a:emptyp data))
    (let* ((max-data-size      (- *buffer-size*
                                  +9p-write-header-fixed-size+))
           (data-chunk-num    (floor (/ (length data) max-data-size)))
           (data-chunk-length (if (> (length data) max-data-size)
                                  max-data-size
                                  (length data)))
           (remainder         (if (> (length data) max-data-size)
                                  (- (length data)
                                     (* data-chunk-num max-data-size))
                                  0)))
      (flet ((write-chunk (chunk chunk-offset)
               (with-new-tag (tag)
                 (let* ((message (compose-message (make-octects *twrite* 1)
                                                  tag
                                                  fid
                                                  (make-octects chunk-offset 8)
                                                  (make-octects (length chunk) 4)
                                                  chunk)))
                   (append-tag-callback tag callback)
                   (send-message stream message)))))
        (loop for i from 0 below (- (length data) remainder) by data-chunk-length do
          (let ((chunk (subseq data i (+ i data-chunk-length))))
            (write-chunk chunk (+ offset i))))
        (when (> remainder 0)
          (write-chunk (subseq data (- (length data) remainder))
                       (+ offset (- (length data) remainder))))))))

(defmethod 9p-write (stream fid offset (data string)
                     &key
                       (callback #'dummy-callback))
  (9p-write stream fid offset (babel:string-to-octets data) :callback callback))

(defun 9p-walk (stream root-fid new-fid new-name &key (callback #'dummy-callback))
  (if (and (numberp new-name)
           (= 0 new-name))
      (%9p-walk-self stream root-fid new-fid :callback callback)
      (with-new-tag (tag)
        (let* ((message (compose-message (make-octects *twalk* 1)
                                         tag
                                         root-fid
                                         new-fid
                                         (make-octects 1 2)
                                         new-name)))
          (append-tag-callback tag callback)
          (send-message stream message)))))

(defun %9p-walk-self (stream root-fid new-fid &key (callback #'dummy-callback))
  (with-new-tag (tag)
    (let* ((message (compose-message (make-octects *twalk* 1)
                                     tag
                                     root-fid
                                     new-fid
                                     (make-octects 0 2))))
      (append-tag-callback tag callback)
      (send-message stream message))))

(defun 9p-remove (stream fid &key (callback #'dummy-callback))
  (with-new-tag (tag)
    (let* ((message (compose-message (make-octects *tremove* 1)
                                     tag
                                     fid)))
      (append-tag-callback tag callback)
      (send-message stream message))))

(defun 9p-clunk (stream fid &key (callback #'dummy-callback))
  (with-new-tag (tag)
    (let* ((message (compose-message (make-octects *tclunk* 1)
                                     tag
                                     fid)))
      (append-tag-callback tag callback)
      (send-message stream message))))

(defun 9p-stat (stream fid &key (callback #'dummy-callback))
  (with-new-tag (tag)
    (let* ((message (compose-message (make-octects *tstat* 1)
                                     tag
                                     fid)))
      (append-tag-callback tag callback)
      (send-message stream message))))

(defun 9p-read (stream fid offset chunk-length &key (callback #'dummy-callback))
  (with-new-tag (tag)
    (let* ((message (compose-message (make-octects *tread* 1)
                                     tag
                                     fid
                                     (make-octects offset 8)
                                     (make-octects chunk-length 4))))
      (append-tag-callback tag callback)
      (send-message stream message))))

(defun decode-read-reply (data &optional (as-string nil))
  (let ((count    (bytes->int (subseq data 0 4)))
        (raw-data (subseq data 4)))
    (values (if as-string
                (babel:octets-to-string raw-data :errorp nil)
                raw-data)
            count)))

(defun encoded-string-offset (decoded-string)
  (+  (length decoded-string)
      +message-string-length-size+))

(a:define-constant +wstat-ignore-1+ #xff               :test #'=)

(a:define-constant +wstat-ignore-2+ #xffff             :test #'=)

(a:define-constant +wstat-ignore-4+ #xffffffff         :test #'=)

(a:define-constant +wstat-ignore-8+ #xffffffffffffffff :test #'=)

(defun permissions->string (permissions)
  (with-output-to-string (stream)
    (flet ((print-no-permission ()
             (write-sequence "-" stream)))
      (if (= (logand permissions #x4) 4)
          (write-sequence "r" stream)
          (print-no-permission))
      (if (= (logand permissions #x2) 2)
          (write-sequence "w" stream)
          (print-no-permission))
      (if (= (logand permissions #x1) 1)
          (write-sequence "x" stream)
          (print-no-permission)))))

(defun permissions->list (permissions)
  (let ((results '()))
    (when (= (logand permissions #x4) 4)
      (push :read results))
    (when (= (logand permissions #x2) 2)
      (push :write results))
    (when (= (logand permissions #x1) 1)
      (push :execute results))))

(defstruct permissions
  (user)
  (group)
  (others)
  (user-string)
  (group-string)
  (others-string)
  (original-value +wstat-ignore-4+))

(defun decode-permissions (data)
  (let ((others-bits (logand data #x7))
        (group-bits  (logand (ash data -3) #x7))
        (user-bits   (logand (ash data -6))))
    (make-permissions :user           (permissions->list   user-bits)
                      :group          (permissions->list   group-bits)
                      :others         (permissions->list   others-bits)
                      :user-string    (permissions->string user-bits)
                      :group-string   (permissions->string group-bits)
                      :others-string  (permissions->string others-bits)
                      :original-value data)))

(defun encode-permissions (permissions)
   (encode (make-octects-permissions (permissions-original-value permissions))))

(defmethod encode ((object permissions))
  (encode-permissions object))

;;;; from tooter

(defparameter *unix-epoch-difference* (encode-universal-time 0 0 0 1 1 1970 0))

(defun universal->unix (universal)
  (- universal *unix-epoch-difference*))

(defun unix->universal (unix)
  (+ unix *unix-epoch-difference*))

;;;;

(defstruct stat
  (entry-size +wstat-ignore-2+)
  (ktype +wstat-ignore-2+)
  (kdev  +wstat-ignore-4+)
  (entry-type :do-not-care)
  (version +wstat-ignore-4+)
  (path +wstat-ignore-8+)
  (mode (make-permissions))
  (atime +wstat-ignore-4+)
  (mtime +wstat-ignore-4+)
  (size +wstat-ignore-8+)
  (name "")
  (user-id "")
  (group-id "")
  (last-modified-from-id ""))

(defun decode-rstat (data)
  "Decode a serialized file/directory entry in a structure `stat'."
  (flet ((->int (start end)
           (bytes->int (subseq data start end))))
    (let* ((entry-block-size      (->int  0  2))
           (entry-size            (->int  2  4))
           (ktype                 (->int  4  6))
           (kdev                  (->int  6 10))
           (entry-type            (->int 10 11))
           (version               (->int 11 15))
           (path                  (->int 15 23))
           (mode                  (->int 23 27))
           (atime                 (->int 27 31))
           (mtime                 (->int 31 35))
           (size                  (->int 35 43))
           (strings-start         43)
           (name                  (decode-string (subseq data strings-start)))
           (name-offset           (encoded-string-offset name))
           (user-id               (decode-string (subseq data
                                                         (+ strings-start
                                                            name-offset))))
           (user-id-offset        (+ strings-start
                                     (encoded-string-offset user-id)
                                     name-offset))
           (group-id              (decode-string (subseq data user-id-offset)))
           (group-id-offset       (+ user-id-offset
                                     (encoded-string-offset group-id)))
           (last-modified-from-id (decode-string (subseq data group-id-offset))))
      (declare (ignore entry-block-size))
      (make-stat :entry-size            entry-size
                 :ktype                 ktype
                 :kdev                  kdev
                 :entry-type            (file-type-number->symbol entry-type)
                 :version               version
                 :path                  path
                 :mode                  (decode-permissions mode)
                 :atime                 (unix->universal atime)
                 :mtime                 (unix->universal mtime)
                 :size                  size
                 :name                  name
                 :user-id               user-id
                 :group-id              group-id
                 :last-modified-from-id last-modified-from-id))))

(defmethod encode ((object stat))
  (flet ((->octects (data size)
           (encode (make-octects data size))))
    (let* ((ktype                 (->octects (stat-ktype object) 2))
           (kdev                  (->octects (stat-kdev object) 4))
           (entry-type            (->octects (symbol->file-type-number (stat-entry-type object))
                                             1))
           (version               (->octects (stat-version object) 4))
           (path                  (->octects (stat-path object) 8))
           (mode                  (encode (stat-mode object)))
           (atime                 (->octects (universal->unix (stat-atime object)) 4))
           (mtime                 (->octects (universal->unix (stat-mtime object)) 4))
           (size                  (->octects (stat-size object) 8))
           (name                  (encode-string (stat-name object)))
           (user-id               (encode-string (stat-user-id object)))
           (group-id              (encode-string (stat-group-id object)))
           (last-modified-from-id (encode-string (stat-last-modified-from-id object)))
           (partial-results       (reduce #'vcat
                                          (list ktype
                                                kdev
                                                entry-type
                                                version
                                                path
                                                mode
                                                atime
                                                mtime
                                                size
                                                name
                                                user-id
                                                group-id
                                                last-modified-from-id)))
           (entry-size             (->octects (length partial-results) 2)))
      (vcat entry-size partial-results))))

(defun 9p-auth (stream authorization-fid username root &key (callback #'dummy-callback))
  (with-new-tag (tag)
    (let ((message (compose-message (make-octects *tauth* 1)
                                    tag
                                    authorization-fid
                                    username
                                    root)))
      (append-tag-callback tag callback)
      (send-message stream message)
      (read-all-pending-messages stream)
      authorization-fid)))

(defgeneric 9p-wstat (stream fid stat &key callback))

(defmethod 9p-wstat (stream fid (stat stat) &key (callback #'dummy-callback))
  (let ((encoded-stat (encode stat)))
    (9p-wstat stream
              fid
              (cat-reply-vector (int16->bytes (length encoded-stat))
                                encoded-stat)
              :callback callback)))

(defmethod 9p-wstat (stream fid (stat vector) &key (callback #'dummy-callback))
  (with-new-tag (tag)
    (let ((message (compose-message (make-octects *twstat* 1)
                                    tag
                                    fid
                                    stat)))
      (append-tag-callback tag callback)
      (send-message stream message)
      fid)))

;;; high level routines

(defun mount (stream root-path &key (username nil) (authorization-handshake nil))
  "Initialize the  connection to the  server using `root-path'  as the
root of the remote file system

If `username' is not nil  `authorization-handshake' must be a function
of two parameters

 - stream
 - authorization-fid

The user can write and  read to authorization-fid using `9p-write' or
`9p-read'  to be  authorized. If  authorized `authorization-handshake'
must  returns  a  non nil  value.  If  returns  a  nil value  this  is
interpreted as access not granted and `mount' returns nil.

If mount  has success  two values  are returned: the  fid of  the root
file system and the protocol version."
  (with-new-fid (authorization-fid)
    (let* ((actual-authorization-fid (if (null username)
                                         +nofid+
                                         authorization-fid))
           (authorizedp              (null username)))
      (when authorization-handshake
        (9p-auth stream actual-authorization-fid username root-path)
        (setf authorizedp (funcall authorization-handshake stream authorization-fid)))
      (if authorizedp
          (let ((protocol-version (initialize-session stream))
                (root-fid         (9p-attach stream root-path)))
            (read-all-pending-messages stream)
            (values root-fid protocol-version))
          (error '9p-authorization-error
                 :connection-data (format nil
                                          "path: ~a username ~a authorization function: ~a"
                                          root-path
                                          username
                                          authorization-handshake))))))

(defun read-all-pending-messages-ignoring-errors (stream)
  "Read all the messages from the server and ignore any error"
  (handler-bind ((9p-error
                   (lambda (e)
                     (invoke-restart 'ignore-error e))))
    (read-all-pending-messages stream)))

(defun clone-fid (stream fid)
  "Make a new fid that points to the same resource pointed by `fid'"
  (with-new-fid (cloned-fid)
    (9p-walk stream fid cloned-fid +nwname-clone+)
    (read-all-pending-messages stream)
    cloned-fid))

(defun create-directory (stream parent-fid directory-name &key (permissions #o750))
  "Create  a  new  directory  with  name  `directory-name'  under  the
directory pointed  by `parent-fid'.  The parameter  `permissions' must
be provided in octal (default: #o760))"
  (with-new-fid (saved-parent-dir)
    (9p-walk stream parent-fid saved-parent-dir +nwname-clone+)
    (read-all-pending-messages stream)
    (9p-create stream
               (clone-fid stream parent-fid)
               directory-name
               :permissions (logior +create-dir+ permissions)
               :mode        +create-for-read+)
    (read-all-pending-messages stream)
    (with-new-fid (new-dir-fid)
      (9p-walk stream saved-parent-dir new-dir-fid directory-name)
      (read-all-pending-messages stream)
      new-dir-fid)))

(defun path-info (stream root-fid path)
  "If returns non nil values a stat structure representing the element
pointed by `path' and a list  of stats structures corresponding to the
directory of `path'."
  (let* ((cloned-root-fid (clone-fid stream root-fid))
         (path-components (remove "/" (fs:split-path-elements path) :test #'string=))
         (got-error       nil))
    (labels ((walk (path-components parent-fid  &optional (accum ()))
               (cond
                 ((null path-components)
                  (9p-clunk stream parent-fid)
                  (read-all-pending-messages stream)
                  accum)
                 (t
                  (let ((results nil))
                    (with-new-fid (path-fid)
                      (handler-case
                          (progn
                            (9p-walk stream parent-fid path-fid (first path-components))
                            (read-all-pending-messages stream))
                        (9p-error (e)
                          (setf got-error t)
                          (ignore-response (tag e))
                          (walk nil parent-fid nil)))
                      (when (null got-error)
                        (9p-clunk stream parent-fid)
                        (read-all-pending-messages stream)
                        (9p-stat stream
                                 path-fid
                                 :callback (lambda (x data)
                                             (declare (ignore x))
                                             (setf results (decode-rstat data))))
                        (read-all-pending-messages stream)
                        (walk (rest path-components) path-fid (push results accum)))))))))
      (let ((stats (walk path-components cloned-root-fid)))
        (values (first stats) (reverse stats))))))

(defun path-exists-p (stream root-fid path)
  "Returns non nil if `path' exists."
  (path-info stream root-fid path))

(defun create-path (stream parent-fid path &key (file-permissions #o640))
  "Create a file or directory as specified by `path'.
To create a  directory specify a path terminating with  a '/'.  Parent
directories are created if do  not exist.  The parameter `permissions'
must be provided in octal (default: #o640))

Returns the fid to the last element of `path'

If the last element of `path'  is a directory the directory is created
with permissions: #o760.

It `path'  already reference a  valid file it  is opened for  read and
write.

Finally if already reference a valid directory is opened for read only."
  (let ((fs:*directory-sep-regexp* "\\/")
        (path-elements             (remove "/"
                                           (fs:split-path-elements path)
                                           :test #'string=))
        (last-is-dir-p             (cl-ppcre:scan "\\/$" path))
        (last-dir-fid              nil)
        (cloned-parent-fid         (clone-fid stream parent-fid)))
    (labels ((%create-dirs (parent-dir-fid path-elements)
               (when path-elements
                 (let* ((next-path   (first path-elements))

                        (new-dir-fid (if (path-exists-p stream
                                                        parent-dir-fid
                                                        next-path)
                                         (open-path stream
                                                    parent-dir-fid
                                                    next-path
                                                    :just-walk t)
                                         (create-directory stream
                                                           parent-dir-fid
                                                           next-path))))
                   (setf last-dir-fid new-dir-fid)
                   (9p-clunk stream parent-dir-fid)
                   (read-all-pending-messages stream)
                   (%create-dirs new-dir-fid (rest path-elements))))))
      (if (> (length path-elements) 1)
          (%create-dirs parent-fid (misc:safe-all-but-last-elt path-elements))
          (setf last-dir-fid parent-fid))
      (let ((path-last-element (a:last-elt path-elements)))
        (cond
          ((path-exists-p stream
                          last-dir-fid
                          path-last-element)
           (9p-clunk stream last-dir-fid)
           (read-all-pending-messages stream)
           (if last-is-dir-p
               (open-path stream cloned-parent-fid path :mode +create-for-read+)
               (open-path stream cloned-parent-fid path :mode +create-for-read-write+)))
          (last-is-dir-p
           (create-directory stream last-dir-fid path-last-element))
          (t
           (9p-create stream
                      last-dir-fid
                      path-last-element
                      :permissions file-permissions)
           (read-all-pending-messages stream)
           last-dir-fid))))))

(defun open-path (stream root-fid path
                  &key
                    (walk-callback #'dummy-callback)
                    (open-callback #'dummy-callback)
                    (mode          +create-for-read+)
                    (just-walk     nil))
  "Open a full, existing, path relative to `root-fid'.  `Mode' must be
  one         of        +create-for-read+,         +create-for-write+,
  +create-for-read-write+"
  (let ((fs:*directory-sep-regexp* "\\/")
        (path-elements             (remove "/"
                                           (fs:split-path-elements path)
                                           :test #'string=)))
    (labels ((walk-dirs (path-elements parent-fid)
                 (if path-elements
                     (with-new-fid (fid)
                       (9p-walk stream
                                parent-fid
                                fid
                                (first path-elements)
                                :callback walk-callback)
                       (9p-clunk stream parent-fid)
                       (read-all-pending-messages stream)
                       (walk-dirs (rest path-elements) fid))
                     parent-fid)))
      (let* ((cloned-root-fid (clone-fid stream root-fid))
             (fid (walk-dirs path-elements cloned-root-fid)))
          (read-all-pending-messages stream)
          (when (not just-walk)
            (9p-open stream fid :callback open-callback :mode mode)
            (read-all-pending-messages stream))
          fid))))

(defun cat-reply-vector (a b)
  (concatenate '(vector (unsigned-byte 8)) a b))

(defun exhausted-stream-for-read-p (count)
  (zerop count))

(defmacro with-callback-read-all ((recurse-function-name offset data-reply byte-count)
                                  &body body)
  `(lambda (x ,data-reply)
     (declare (ignore x))
     (multiple-value-bind (,data-reply ,byte-count)
         (decode-read-reply ,data-reply nil)
       ,@body
       (when (not (exhausted-stream-for-read-p ,byte-count))
         (,recurse-function-name (+ ,offset ,byte-count))))))

(defun maximum-read-buffer-size ()
  (- *buffer-size*
     +9p-read-header-fixed-size+
     1))

(defun read-entire-file-apply-function (stream root-fid
                                        path
                                        read-function
                                        &key (buffer-size (maximum-read-buffer-size)))
  "Read an entire file pointed by `path' applying the function `read-function'
to three arguments:

- the data chunk as a vector of (unsigned-byte 8);
- the offset to the start of the chunk red;
- the length of the chunk.

Note: `path' is relative to `root-fid'

Note also  that a  failed assertion is  signalled if  `buffer-size' is
bigger than:

*buffer-size* - +9p-read-header-fixed-size+"
  (assert (< buffer-size
             (- *buffer-size*
                +9p-read-header-fixed-size+)))
  (let ((fid (open-path stream root-fid path)))
    (labels ((slurp (offset)
               (declare (optimize (debug 0) (speed 3)))
               (9p-read stream
                        fid
                        offset
                        buffer-size
                        :callback (with-callback-read-all (slurp offset data count)
                                      (funcall read-function data offset count)))))
      (slurp 0)
      (read-all-pending-messages stream))))

(defun slurp-file (stream root-fid path &key (buffer-size (maximum-read-buffer-size)))
  "Read in a vector of (unsigned-byte 8) the full content of
'path' relative to `root-fid'

Note: a failed assertion is signalled if `buffer-size' is bigger than:

*buffer-size* - +9p-read-header-fixed-size+"
  (let ((res (make-array 0 :element-type +byte-type+ :adjustable nil)))
    (read-entire-file-apply-function stream
                                     root-fid
                                     path
                                     (lambda (data offset count)
                                       (declare (ignore offset count))
                                       (setf res (cat-reply-vector res data)))
                                     :buffer-size buffer-size)
    res))

(defun remove-path (stream root-fid path)
  "Remove the last element of 'path' relative to `root-fid'.
If `path' does not exists this function does nothing."
  (when (path-exists-p stream root-fid path)
    (let* ((saved-root-fid (clone-fid stream root-fid))
           (path-fid       (open-path stream saved-root-fid path)))
      (9p-remove stream path-fid))
    (read-all-pending-messages stream)))

(defun open-directory (stream root-fid path)
  "Open the directory pointed to `path' relative to `root-fid'.
Returns the fid of the directory pointed by `path'.

This  fid  can  be  used  by `read-directory`  to  get  the  directory
entries (see the struct `stat')"
  (let* ((root-fid-cloned (clone-fid stream root-fid))
         (path-fid        (open-path stream root-fid-cloned path)))
    (9p-clunk stream root-fid-cloned)
    path-fid))

(defun read-directory (stream dir-fid &optional (offset 0))
  "Read the next entry of a directory specified by `dir-fid'.

`Dir-fid' is usually the values returned by `open-directory`.

This function on success returns tree values:

- dir-fid to  read the  next entry or  to check if  there are  no more
  entries (see below);
- the stat struct for the current entry;
- an offset to the next entry.

Each call to this function on the same directory fid, except the first, must use the offset returned by the last call, for example

(let* ((dir-fid (open-directory stream root-fid path))
       (multiple-value-bind (next-dir-fid stat stat-size)
           (read-directory stream dir-fid) ; offset 0 on the first call
         (multiple-value-bind (next-dir-fid-2 stat-2 next-stat-size-2)
             (read-directory stream dir-fid stat-size) ; offset equals
                                                       ; to  stat-size
                                                       ; from previous
                                                       ; call
           ...))))

When  this  function  returns  nil  there no  more  entries  for  this
directory (see the code of directory children)."
  (flet ((cut-away-reply-length (data)
           (subseq data 4)))
    (let* ((stat-size              nil)
           (stat-size-bytes        nil)
           (stat-size-bytes-padded nil)
           (rstat                  nil)
           (stat-padding           #(0 0)))
      (9p-read stream
               dir-fid
               offset  2
               :callback (lambda (x data)
                           (declare (ignore x))
                           (setf stat-size-bytes  (cut-away-reply-length data))
                           (setf stat-size (bytes->int stat-size-bytes))
                           (setf stat-size-bytes-padded (cat-reply-vector stat-padding
                                                                          stat-padding))))
      (read-all-pending-messages stream)
      (when (> stat-size 0)
        (9p-read stream
                 dir-fid
                 (+ offset 2) stat-size
                 :callback
                 (lambda (x data)
                   (declare (ignore x))
                   (setf rstat
                         (decode-rstat (cat-reply-vector stat-size-bytes-padded
                                                         (cut-away-reply-length data))))))
        (read-all-pending-messages stream)
        (values dir-fid rstat (+ offset 2 stat-size))))))

(defun sort-dir-stats (data &optional (fn (lambda (a b) (string< (stat-name a) (stat-name b)))))
  "Sort a list  of stat struct `data' (by default  sorting entry names
in lexicographic ascending order"
  (sort data fn))

(defun collect-directory-children (stream root-fid path)
  "Collect all the directory entries  of `path' relative to `root-fid'
as a list  of `stat` structures."
  (let* ((dir-fid   (open-directory stream root-fid path))
         (dir-stats '()))
    (loop named collecting-loop
          with stat-size = 0
          do
             (multiple-value-bind (next-dir-fid stat next-stat-size)
                 (read-directory stream dir-fid stat-size)
               (if next-dir-fid
                   (progn
                     (push stat dir-stats)
                     (setf stat-size next-stat-size))
                   (return-from collecting-loop t))))
    (9p-clunk stream dir-fid)
    (sort-dir-stats dir-stats)))

(defun collect-tree (stream root-fid starting-directory)
  "Traverse  the file  system  tree  starting at  `starting-directory'
returning two values
 - a list of strings representing  the paths of all the *files*
   under `starting-directory'
 - a list of all the path to the sub-directories under `starting-directory'

The lists are ordered in topological order (depth first strategy)."
  (let* ((starting-root-fid (clone-fid stream root-fid)))
    (labels ((%collect-tree (stream a-root-fid unvisited-dirs
                             &optional
                               (accum-files '())
                               (accum-directories '()))
               ;; note that after this function is called a-root-fid is no more valid
               (declare (optimize (debug 0) (speed 3)))
               (cond
                 ((null unvisited-dirs)
                  (9p-clunk stream a-root-fid)
                  (values accum-files accum-directories))
                 (t
                  (flet ((dirp (a)
                           (eq (stat-entry-type a)
                               :directory))
                         (prepend-parent-path-clsr (path)
                           (lambda (name)
                             (fs:cat-parent-dir path (stat-name name)))))
                    (let* ((cloned-root-fid  (clone-fid stream a-root-fid)))
                      (let* ((path             (first unvisited-dirs))
                             (path-fid         (open-path stream cloned-root-fid path))
                             (children         (collect-directory-children stream
                                                                           cloned-root-fid
                                                                           path))
                             (files            (mapcar (prepend-parent-path-clsr path)
                                                       (remove-if #'dirp children)))
                             (directories      (remove-if-not #'dirp children))
                             (directories-path (mapcar (prepend-parent-path-clsr path)
                                                       directories)))
                        (9p-clunk stream a-root-fid)
                        (9p-clunk stream path-fid)
                        (read-all-pending-messages stream)
                        (%collect-tree stream
                                       cloned-root-fid
                                       (append (rest unvisited-dirs) directories-path)
                                       (append files accum-files)
                                       (append directories-path accum-directories)))))))))
      (multiple-value-bind (files directories)
          (%collect-tree stream starting-root-fid (list starting-directory))
        (values files directories)))))

(defun copy-file (stream root-fid path-from path-to
                  &key
                    (buffer-size (maximum-read-buffer-size))
                    (progress-fn (lambda (count) (declare (ignore count)))))
  "Copy a file pointed by `path-from' to a file pointed by `path-to'.
   `progress-fn',  if  defined,  must  be a  function  with  a  single
   parameter: the  number of  octects wrote  in a  single call  of the
   Twrite command.

   Note: the destination file is overwritten if does exists."
  (let* ((root-fid-destination (clone-fid stream root-fid))
         (root-fid-origin      (clone-fid stream root-fid))
         (destination-fid (create-path stream root-fid-destination path-to))
         (origin-fid      (open-path   stream root-fid-origin path-from)))
    (labels ((slurp (offset)
               (declare (optimize (debug 0) (speed 3)))
               (9p-read stream
                        origin-fid
                        offset
                        buffer-size
                        :callback (with-callback-read-all (slurp offset data count)
                                    (when (not (exhausted-stream-for-read-p count))
                                      (9p-write stream destination-fid offset data)
                                      (funcall progress-fn count))))))
      (slurp 0)
      (read-all-pending-messages stream)
      (let* ((path-from-stat (path-info stream root-fid path-from))
             (from-mode      (permissions-original-value (stat-mode path-from-stat))))
        (change-mode stream root-fid path-to from-mode))
      (9p-clunk stream destination-fid)
      (9p-clunk stream origin-fid)
      (read-all-pending-messages stream))))

(defun move-file (stream root-fid path-from path-to
                  &key
                    (buffer-size (maximum-read-buffer-size))
                    (progress-fn (lambda (count) (declare (ignore count)))))
  "Move the file from `path-from` `path-to`, this function can be used
to rename a file too."
  (copy-file stream
             root-fid
             path-from
             path-to
             :buffer-size buffer-size
             :progress-fn progress-fn)
  (read-all-pending-messages stream)
  (let ((source-fid (open-path stream root-fid path-from)))
    (9p-remove stream source-fid)
    (read-all-pending-messages stream)))

(defun truncate-file (stream root-fid path &key (new-size 0) (callback #'dummy-callback))
  "Truncate the  size of  the element  pointed by  `path' to  the size
indicated by the value of `new-size' (default 0). Note that only files
could be truncated, not directory."
  (let* ((cloned-root-fid (clone-fid stream root-fid))
         (fid             (open-path stream cloned-root-fid path :mode +create-for-write+))
         (stat            (make-stat)))
    (setf (stat-size stat) new-size)
    (9p-wstat stream fid stat :callback callback)
    (read-all-pending-messages stream)
    (9p-clunk stream fid)
    (9p-clunk stream cloned-root-fid)
    (read-all-pending-messages stream)))

(defun change-time (stream root-fid path time slot-function-name &key (callback #'dummy-callback))
  (let* ((cloned-root-fid (clone-fid stream root-fid))
         (fid             (open-path stream cloned-root-fid path :mode +create-for-write+))
         (stat            (make-stat)))
    (setf (slot-value stat slot-function-name) time)
    (9p-wstat stream fid stat :callback callback)
    (read-all-pending-messages stream)
    (9p-clunk stream fid)
    (9p-clunk stream cloned-root-fid)
    (read-all-pending-messages stream)))

(defun change-access-time (stream root-fid path time &key (callback #'dummy-callback))
  (change-time stream root-fid path time 'atime :callback callback))

(defun change-modify-time (stream root-fid path time &key (callback #'dummy-callback))
  (change-time stream root-fid path time 'mtime :callback callback))

(defun change-time-values (stream root-fid path
                           new-access-time
                           new-modification-time
                           &key (callback #'dummy-callback))
  (let* ((cloned-root-fid (clone-fid stream root-fid))
         (fid             (open-path stream cloned-root-fid path :mode +create-for-write+))
         (stat            (make-stat)))
    (setf (stat-atime stat) new-access-time)
    (setf (stat-mtime stat) new-modification-time)
    (9p-wstat stream fid stat :callback callback)
    (read-all-pending-messages stream)
    (9p-clunk stream fid)
    (9p-clunk stream cloned-root-fid)
    (read-all-pending-messages stream)))

(defun change-mode (stream root-fid path new-mode &key (callback #'dummy-callback))
  (let* ((cloned-root-fid (clone-fid stream root-fid))
         (fid             (open-path stream cloned-root-fid path :mode +create-for-write+))
         (stat            (make-stat)))
    (setf (stat-mode stat) (make-octects-permissions new-mode))
    (9p-wstat stream fid stat :callback callback)
    (read-all-pending-messages stream)
    (9p-clunk stream fid)
    (9p-clunk stream cloned-root-fid)
    (read-all-pending-messages stream)))

(defun change-permissions (stream root-fid path new-mode &key (callback #'dummy-callback))
  (change-mode stream root-fid path new-mode :callback callback))
