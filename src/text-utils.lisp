;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :purgatory.text-utils)

(defun strcat (&rest chunks)
  (declare (optimize (debug 0) (safety 0) (speed 3)))
  (strcat* chunks))

(defun strcat* (chunks)
  (declare (optimize (debug 0) (safety 0) (speed 3)))
  (reduce (lambda (a b) (concatenate 'string a b)) chunks))
