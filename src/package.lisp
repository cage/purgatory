;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(defpackage :purgatory.text-utils
  (:use
   :cl)
  (:export
   :strcat))

(defpackage :purgatory.misc-utils
  (:use :cl)
  (:export
   :safe-all-but-last-elt))

(defpackage :purgatory.filesystem-utils
  (:use
   :cl)
  (:local-nicknames (:a :alexandria))
  (:export
   :*directory-sep-regexp*
   :getenv
   :parent-dir-path
   :split-path-elements
   :path-last-element
   :backreference-dir-p
   :loopback-reference-dir-p
   :path-referencing-dir-p
   :cat-parent-dir))

(defpackage :purgatory
  (:use
   :cl)
  (:local-nicknames (:fs   :purgatory.filesystem-utils)
                    (:misc :purgatory.misc-utils)
                    (:text :purgatory.text-utils)
                    (:a    :alexandria))
  (:export
   :+byte-type+
   :+version+
   :+nofid+
   :+create-for-read+
   :+create-for-write+
   :+create-for-read-write+
   :+create-for-exec+
   :+create-dir+
   :+open-truncate+
   :+open-remove-on-clunk+
   :+standard-socket-port+
   :+nwname-clone+
   :*tag*
   :*fid*
   :*buffer-size*
   :*messages-sent*
   :9p-error
   :ignore-error
   :read-all-pending-messages
   :close-client
   :encode-string
   :decode-string
   :encode
   :decode
   :read-message
   :initialize-session
   :with-new-tag
   :with-new-fid
   :dummy-callback
   :dump-callback
   :9p-attach
   :9p-create
   :9p-open
   :9p-write
   :9p-auth
   :9p-remove
   :9p-clunk
   :9p-stat
   :9p-read
   :9p-walk
   :decode-read-reply
   :make-permissions
   :permissions-user
   :permissions-group
   :permissions-others
   :permissions-user-string
   :permissions-group-string
   :permissions-others-string
   :permissions-original-value
   :make-stat
   :stat-entry-size
   :stat-ktype
   :stat-kdev
   :stat-entry-type
   :stat-version
   :stat-path
   :stat-mode
   :stat-atime
   :stat-mtime
   :stat-size
   :stat-name
   :stat-user-id
   :stat-group-id
   :stat-last-modified-from-id
   :decode-rstat
   :read-all-pending-messages-ignoring-errors
   :clone-fid
   :create-directory
   :path-exists-p
   :path-info
   :create-path
   :mount
   :open-path
   :read-entire-file-apply-function
   :slurp-file
   :remove-path
   :open-directory
   :read-directory
   :sort-dir-stats
   :collect-directory-children
   :collect-tree
   :copy-file
   :move-file
   :truncate-file
   :change-access-time
   :change-modify-time
   :change-time-values
   :change-mode
   :change-permissions))
