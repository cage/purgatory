;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :purgatory)

(defparameter *tversion*  100)

(defparameter *rversion*  101)

(defparameter *tauth*     102)

(defparameter *rauth*     103)

(defparameter *tattach*   104)

(defparameter *rattach*   105)

(defparameter *terror*    106) ; there is no terror

(defparameter *rerror*    107)

(defparameter *tflush*    108)

(defparameter *rflush*    108)

(defparameter *twalk*     110)

(defparameter *rwalk*     109)

(defparameter *topen*     112)

(defparameter *ropen*     113)

(defparameter *tcreate*   114)

(defparameter *rcreate*   115)

(defparameter *tread*     116)

(defparameter *rread*     117)

(defparameter *twrite*    118)

(defparameter *rwrite*    119)

(defparameter *tclunk*    120)

(defparameter *rclunk*    121)

(defparameter *tremove*   122)

(defparameter *rremove*   123)

(defparameter *tstat*     124)

(defparameter *rstat*     125)

(defparameter *twstat*    126)

(defparameter *rwstat*    127)
