;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :purgatory.filesystem-utils)

(a:define-constant +file-path-regex+ "[\\p{L},\\/,\\\\,\\.]+"    :test 'string=)

(defparameter *directory-sep-regexp*
  #+windows "\\"
  #-windows "\\/")

(defparameter *directory-sep*
  #+windows "\\"
  #-windows "/")

(defun getenv (name)
  (uiop:getenv name))

(defun split-path-elements (path)
  (let ((splitted (cl-ppcre:split *directory-sep-regexp* path)))
    (substitute *directory-sep* "" splitted :test #'string=)))

(defun parent-dir-path (path)
  (let ((splitted (remove-if #'(lambda (a) (string= "" a))
                             (split-path-elements path))))
    (cond
      ((> (length splitted) 1)
       (let ((res (if (string= (string (elt path 0)) *directory-sep*)
                      (concatenate 'string *directory-sep* (first splitted))
                      (first splitted))))
         (loop for i in (subseq splitted 1 (1- (length splitted))) do
              (setf res (concatenate 'string res *directory-sep* i)))
         (setf res (concatenate 'string res *directory-sep*))
         res))
      ((or (= (length splitted) 1)
           (null splitted))
       *directory-sep*)
      (t
       path))))

(defun path-last-element (path)
  (let ((elements (cl-ppcre:split *directory-sep-regexp* path)))
    (and elements
         (a:last-elt elements))))

(defun backreference-dir-p (path)
  (string= (path-last-element path) ".."))

(defun loopback-reference-dir-p (path)
  (string= (path-last-element path) "."))

(defun path-referencing-dir-p (path)
  (cl-ppcre:scan "/$" path))

(defun cat-parent-dir (parent direntry)
  (cond
    ((or (backreference-dir-p direntry)
         (loopback-reference-dir-p direntry))
     (format nil "~a~a" parent direntry))
    ((string= (string (a:last-elt parent)) *directory-sep*)
     (format nil "~a~a" parent direntry))
    (t
      (format nil "~a~a~a" parent *directory-sep* direntry))))
