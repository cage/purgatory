;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :purgatory)

(define-condition 9p-error (error)
  ((error-value
    :initarg :error-value
    :reader error-value)
   (message-type
    :initarg :message-type
    :reader message-type)
   (tag
    :initarg :tag
    :reader tag))
  (:report (lambda (condition stream)
             (format stream
                     "message-type ~a tag ~a: ~a"
                     (message-type condition)
                     (tag condition)
                     (error-value condition))))
  (:documentation "Error for 9p protocol"))

(define-condition 9p-initialization-error (error)
  ((tag
    :initarg :tag
    :reader tag)
   (rtag
    :initarg :rtag
    :reader rtag))
  (:report (lambda (condition stream)
             (format stream "error initialization tag sent ~a, got ~a instead"
                     (tag condition) (rtag condition))))
  (:documentation "Error for 9p protocol in the initialization phase."))

(define-condition 9p-authorization-error (error)
  ((connection-string
    :initarg :connection-data
    :reader connection-data))
  (:report (lambda (condition stream)
             (format stream
                     "Authorization failed. Connection data: ~a"
                     (connection-data condition))))
  (:documentation "Error for 9p protocol in the authorization phase."))
