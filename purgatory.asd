;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(defsystem :purgatory
  :description "A simple implementation of the 9p filesystem protocol."
  :author      "cage"
  :license     "LLGPL"
  :version     "0.1.2"
  :encoding    :utf-8
  :maintainer  "cage"
  :bug-tracker "https://codeberg.org/cage/purgatory/issues"
  :serial      t
  :pathname    "src"
  :depends-on (:alexandria
               :cl-ppcre
               :usocket
               :babel
               :uiop)
  :components ((:file "package")
               (:file "text-utils")
               (:file "misc-utils")
               (:file "filesystem-utils")
               (:file "conditions")
               (:file "message-types")
               (:file "client")))
