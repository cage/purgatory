;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(defpackage :all-tests
  (:use :cl
        :clunit
        :cl+ssl)
  (:local-nicknames (:fs   :purgatory.filesystem-utils)
                    (:misc :purgatory.misc-utils))
  (:export
   :*client-certificate*
   :*certificate-key*
   :*host*
   :*port*
   :with-open-ssl-stream
   :all-suite
   :run-all-tests))

(defpackage :protocol-tests
  (:use :cl
        :clunit
        :purgatory.text-utils
        :purgatory
        :all-tests)
  (:local-nicknames (:fs   :purgatory.filesystem-utils)
                    (:misc :purgatory.misc-utils))
  (:export))
