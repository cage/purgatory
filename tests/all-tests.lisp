;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :all-tests)

(defparameter *client-certificate* "/home/cage/lisp/tinmop/kamid.cert")

(defparameter *certificate-key*    "/home/cage/lisp/tinmop/kamid.key")

(defparameter *host*               "localhost")

(defparameter *port*               10564)

(defun open-tls-socket (host port)
  (flet ((open-socket (hostname)
           (usocket:socket-connect hostname
                                   port
                                   :element-type '(unsigned-byte 8))))
    (or (ignore-errors (open-socket host))
        (open-socket host))))

(defmacro with-open-ssl-stream ((ssl-stream socket host port
                                 client-certificate
                                 certificate-key)
                                &body body)
  (alexandria:with-gensyms (tls-context socket-stream ssl-hostname)
    `(let ((,tls-context (cl+ssl:make-context :verify-mode cl+ssl:+ssl-verify-none+)))
       (cl+ssl:with-global-context (,tls-context :auto-free-p t)
         (let* ((,socket        (open-tls-socket ,host ,port))
                (,socket-stream (usocket:socket-stream ,socket))
                (,ssl-hostname  ,host)
                (,ssl-stream
                  (cl+ssl:make-ssl-client-stream ,socket-stream
                                                 :certificate     ,client-certificate
                                                 :key             ,certificate-key
                                                 :external-format nil ; unsigned byte 8
                                                 :unwrap-stream-p t
                                                 :verify          nil
                                                 :hostname        ,ssl-hostname)))
           ,@body)))))

(defsuite all-suite ())

(defun run-all-tests (&key (use-debugger t))
  (setf *client-certificate* (or *client-certificate*
                                 (fs:getenv "REGRESS_CERT"))
        *certificate-key*    (or *certificate-key*
                                 (fs:getenv "REGRESS_KEY"))
        *host*               (or *host*
                                 (fs:getenv "REGRESS_HOSTNAME"))
        *port*               (or *port*
                                 (parse-integer (fs:getenv "REGRESS_PORT"))))
  (clunit:run-suite 'all-suite :use-debugger use-debugger :report-progress t))
