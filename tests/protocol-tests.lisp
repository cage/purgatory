;; This software is Copyright ©  2021 cage
;; cage grants you the rights to distribute
;; and use this software as governed by the terms
;; of the Lisp Lesser GNU Public License
;; (http://opensource.franz.com/preamble.html),
;; known as the LLGPL

(in-package :protocol-tests)

(defparameter *remote-test-file*         "test-file") ; note: missing "/" is intentional

(defparameter *remote-test-path*         "/test-file")

(defparameter *remote-test-path-write*   "/dir/subdir/test-file-write")

(defparameter *remote-test-path-huge*    "/test-file-huge")

(defparameter *remote-test-path-contents* (format nil "qwertyuiopasdfghjklòàù è~%"))

(alexandria:define-constant +remote-test-path-ovewrwrite-data+ "12" :test #'string=)

(defsuite protocol-suite (all-suite))

(defun start-non-tls-socket (host port)
  (usocket:socket-connect host
                          port
                          :protocol     :stream
                          :element-type +byte-type+))

(defun example-mount (&optional (root  "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let ((*messages-sent* '())
          (root-fid        (mount stream root)))
      (9p-clunk stream root-fid)
      (read-all-pending-messages stream)
      (9p-attach stream root)
      (read-all-pending-messages stream)
      t)))

(deftest test-mount (protocol-suite)
  (assert-true (ignore-errors (example-mount))))

(defun example-walk (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)

    (let ((*messages-sent* '())
          (root-fid (mount stream root)))
      (with-new-fid (path-fid)
        (9p-walk stream root-fid path-fid path)
        (read-all-pending-messages stream)
        t))))

(deftest test-walk (protocol-suite)
  (assert-true (ignore-errors (example-walk *remote-test-file*))))

(defun example-open-path (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* '())
           (purgatory::*fid* 10)
           (root-fid (mount stream root)))
      (with-new-fid (saved-root-fid)
        (9p-walk stream root-fid saved-root-fid +nwname-clone+)
        (9p-clunk stream (open-path stream saved-root-fid path))
        (read-all-pending-messages stream)
        t))))

(deftest test-open-path (protocol-suite)
  (assert-true (ignore-errors (example-open-path *remote-test-path*))))

(defun example-read (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let ((*messages-sent* ())
          (*buffer-size*   256)
          (root-fid        (mount stream root)))
      (with-new-fid (path-fid)
        (9p-walk stream root-fid path-fid path)
        (9p-open stream path-fid)
        (9p-read stream path-fid 0 10)
        (read-all-pending-messages stream)
        t))))

(deftest test-read ((protocol-suite) (test-walk))
  (assert-true (ignore-errors (example-open-path *remote-test-file*))))

(defun example-slurp (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let ((*messages-sent* ())
          (*buffer-size*   256)
          (root-fid        (mount stream root)))
      (babel:octets-to-string (slurp-file stream
                                          root-fid path
                                          :buffer-size 3)
                              :errorp nil))))

(deftest test-slurp-file ((protocol-suite) (test-read))
  (assert-equality #'string=
      *remote-test-path-contents*
      (example-slurp *remote-test-path*)))

(defun example-write-data (path data &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (*buffer-size*   256)
           (root-fid        (mount stream root))
           (fid             (open-path stream root-fid path :mode +create-for-read-write+)))
      (9p-write stream fid 0 data)
      (read-all-pending-messages stream)
      t)))

(defun example-write (path &optional (root "/"))
  (example-write-data path *remote-test-path-contents* root))

(deftest test-write ((protocol-suite) (test-open-path test-read))
  (assert-true (ignore-errors (example-write *remote-test-path-write*)))
  (assert-true (ignore-errors (example-write-data *remote-test-path-write* #()))))

(defun example-write-2-3 (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (*buffer-size*   256)
           (root-fid        (mount stream root)))
      (with-new-fid (saved-root-fid)
        (9p-walk stream root-fid saved-root-fid +nwname-clone+)
        (let ((fid (open-path stream root-fid path :mode +create-for-read-write+)))
          (9p-write stream fid 2 +remote-test-path-ovewrwrite-data+)
          (read-all-pending-messages stream)
          (babel:octets-to-string (slurp-file stream saved-root-fid path)))))))

(defun read-entire-file-as-string (path  &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (*buffer-size*   256)
           (root-fid        (mount stream root)))
      (babel:octets-to-string (slurp-file stream root-fid path)))))

(deftest test-example-write-2-3 ((protocol-suite) (test-write))
  (example-write-2-3 *remote-test-path-write*)
  (let* ((expected-sequence (copy-seq *remote-test-path-contents*))
         (file-sequence     (read-entire-file-as-string *remote-test-path-write*)))
    (setf (subseq expected-sequence 2 4) +remote-test-path-ovewrwrite-data+)
    (assert-equality #'string= file-sequence expected-sequence)))

(defun example-write-fails (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (*buffer-size*   256)
           (root-fid        (mount stream root))
           (fid             (open-path stream root-fid path :mode +create-for-read-write+)))
      (9p-write stream fid 0 *remote-test-path-contents*)
      (read-all-pending-messages stream))))

(deftest test-write-on-directory-fails ((protocol-suite) (test-write))
  (assert-condition 9p-error (example-write-fails "/")))

(defun example-stat (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (*buffer-size*   256)
           (root-fid        (mount stream root))
           (fid             (open-path stream root-fid path :mode +create-for-read+))
           (results         nil))
      (9p-stat stream fid
               :callback (lambda (x data)
                           (declare (ignore x))
                           (setf results (decode-rstat data))))
      (read-all-pending-messages stream)
      results)))

(deftest test-stat (protocol-suite)
  (assert-true (ignore-errors (example-stat "/")))
  (assert-true (ignore-errors (example-stat *remote-test-path*)))
  (assert-eq   :directory
      (stat-entry-type (example-stat "/")))
  (assert-eq   :file
      (stat-entry-type (example-stat *remote-test-path*))))

(defun example-path-exists (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (path-exists-p stream root-fid path))))

(defun example-path-exists-many-times (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (loop repeat 10000 do
        (path-exists-p stream root-fid path))
        (path-exists-p stream root-fid path))))

(deftest test-path-exists ((protocol-suite) (test-stat))
  (assert-true  (example-path-exists *remote-test-path*))
  (assert-false (example-path-exists (strcat *remote-test-path* ".$$$"))))

(deftest test-path-exists-many-times ((protocol-suite) (test-path-exists))
  (assert-true  (example-path-exists-many-times *remote-test-path*)))

(defun example-create-file (path &optional (root "/"))
  (with-open-ssl-stream (stream
                                       socket
                                       *host*
                                       *port*
                                       *client-certificate*
                                       *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (with-new-fid (saved-root-fid)
        (9p-walk stream root-fid saved-root-fid +nwname-clone+)
        (9p-create stream root-fid path)
        (read-all-pending-messages stream)
        (9p-clunk stream root-fid)
        (open-path stream saved-root-fid path)
        (read-all-pending-messages stream)
        t))))

(alexandria:define-constant +create-file+ "test-file-create" :test #'string=)

(defun example-create-directory (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (create-directory stream root-fid path)
      t)))

(alexandria:define-constant +create-directory+ "test-dir-create" :test #'string=)

(defun example-create-path-read-write (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root))
           (saved-root-fid  (clone-fid stream root-fid))
           (new-path-fid    (create-path stream root-fid path)))
      (9p-write stream new-path-fid 0 *remote-test-path-contents*)
      (read-all-pending-messages stream)
      (9p-clunk stream new-path-fid)
      (read-all-pending-messages stream)
      (babel:octets-to-string (slurp-file stream saved-root-fid path)))))

(defun example-create-path (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (create-path stream root-fid path))))

(alexandria:define-constant +create-path-read-write+ "/a/totaly/new/path/new-file"
  :test #'string=)

(alexandria:define-constant +create-path-dir+ "/this/" :test #'string=)

(alexandria:define-constant +create-path-file+ "/this-file" :test #'string=)

(deftest test-create ((protocol-suite) (test-open-path))
  (assert-true (ignore-errors (example-create-file +create-file+)))
  (assert-true (ignore-errors (example-create-directory +create-directory+)))
  (assert-true (ignore-errors (example-create-path +create-path-dir+)))
  (assert-true (ignore-errors (example-create-path +create-path-file+)))
  (assert-equality #'string=
      *remote-test-path-contents*
      (ignore-errors (example-create-path-read-write +create-path-read-write+))))

(deftest test-create-existing-path ((protocol-suite) (test-create))
  (assert-true (ignore-errors (example-create-path +create-path-read-write+))))

(defun close-parent-fid (&optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (with-new-fid (dir-fid)
        (9p-walk stream root-fid dir-fid "dir")
        (read-all-pending-messages stream)
        (9p-clunk stream root-fid)
        (read-all-pending-messages stream)
        (with-new-fid (subdir-fid)
          (9p-walk stream dir-fid subdir-fid "subdir")
          (read-all-pending-messages stream)
          (9p-clunk stream dir-fid)
          (read-all-pending-messages stream)
          (with-new-fid (file-fid)
            (9p-walk stream subdir-fid file-fid "test-file-write")
            (read-all-pending-messages stream)
            (9p-clunk stream subdir-fid)
            (read-all-pending-messages stream)
            (9p-open stream file-fid)
            (read-all-pending-messages stream)
            t))))))

(deftest test-close-parent-fid ((protocol-suite) (test-walk))
  (assert-true (ignore-errors (close-parent-fid))))

(defun %remove-path (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)

    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (remove-path stream root-fid path)
      t)))

(deftest test-remove-file ((protocol-suite) (test-create-existing-path))
  (assert-true (ignore-errors (%remove-path +create-path-read-write+))))

(deftest test-remove-directory ((protocol-suite) (test-remove-file))
  (assert-true
      (ignore-errors (%remove-path (fs:parent-dir-path +create-path-read-write+)))))

(defun read-dir-same-offset (dir-path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root))
           (root-fid-cloned (clone-fid stream root-fid))
           (dir-fid         (open-path stream root-fid-cloned dir-path))
           (res-read-1      nil)
           (res-read-2      nil))
      (9p-read stream
               dir-fid
               0  10
               :callback (lambda (x data)
                           (declare (ignore x))
                           (setf res-read-1 data)))
      (9p-read stream
               dir-fid
               0  10
               :callback (lambda (x data)
                           (declare (ignore x))
                           (setf res-read-2 data)))
      (read-all-pending-messages stream)
      (not (mismatch res-read-1 res-read-2)))))

(defun example-directory-children (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (collect-directory-children stream root-fid path))))

(deftest test-collect-dir-root-children ((protocol-suite) (test-read))
  (assert-true (example-directory-children "/")))

(defun make-huge-data ()
  (let* ((*random-state* (make-random-state nil)))
    (make-array 1000000
                :element-type '(unsigned-byte 8)
                :initial-contents (loop repeat 1000000
                                        collect
                                        (random 256)))))

(defun write-huge-file (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (*buffer-size*   256)
           (root-fid        (mount stream root))
           (saved-root-fid  (clone-fid stream root-fid))
           (fid             (create-path stream root-fid path))
           (data            (make-huge-data)))
      (9p-write stream fid 0 data)
      (9p-clunk stream fid)
      (read-all-pending-messages stream)
      (path-info stream saved-root-fid path))))

(deftest test-write-huge-file ((protocol-suite) (test-collect-dir-root-children))
  (let* ((size-file  (stat-size (write-huge-file *remote-test-path-huge*))))
    (assert-equality #'= (length (make-huge-data)) size-file)))

(defun read-huge-file (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let ((*messages-sent* ())
          (*buffer-size*   4096)
          (root-fid        (mount stream root)))
      (slurp-file stream
                  root-fid path
                  :buffer-size 3000))))

(deftest test-read-huge-data ((protocol-suite) (test-write-huge-file))
  (assert-equality #'=
      (length (make-huge-data))
      (length (read-huge-file *remote-test-path-huge*))))

(defun read-data-exceeding-msize (path buffer-size &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (*buffer-size*   buffer-size)
           (root-fid        (mount stream root))
           (path-fid        (open-path stream root-fid path))
           (results         nil))
      (9p-read stream
               path-fid
               0
               (* 2 buffer-size)
               :callback (lambda (x reply)
                           (declare (ignore x))
                           (let ((data (decode-read-reply reply nil)))
                             (setf results data))))
      (read-all-pending-messages stream)
      results)))

(deftest test-read-a-tiny-amount-of-data ((protocol-suite) (test-write-huge-file))
  (let ((buffer-size 256))
    (assert-condition 9p-error
        (read-data-exceeding-msize *remote-test-path-huge* buffer-size))))

(defun example-copy-file (from to &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (copy-file stream root-fid from to)
      (slurp-file stream root-fid to))))

(deftest test-copy-file ((protocol-suite) (test-write-huge-file))
  (assert-equality #'equalp
      (make-huge-data)
      (example-copy-file *remote-test-path-huge*
                         (concatenate 'string
                                      *remote-test-path-huge*
                                      "-copy"))))

(defun example-move-file (from to &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (move-file stream root-fid from to)
      (path-exists-p stream root-fid from))))

(defun renamed-filename ()
  (concatenate 'string *remote-test-path-huge* "-renamed"))

(deftest test-move-file ((protocol-suite) (test-copy-file))
  (assert-false (example-move-file *remote-test-path-huge* (renamed-filename))))

(alexandria:define-constant +truncate-size+ 128 :test #'=)

(defun example-truncate-file (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (truncate-file stream root-fid path :new-size +truncate-size+)
      (stat-size (path-info stream root-fid path)))))

(deftest test-truncate-file ((protocol-suite) (test-move-file))
  (assert-equality #'=
      +truncate-size+
      (example-truncate-file (renamed-filename))))

(alexandria:define-constant +new-atime+   (encode-universal-time 0 0 10 22 10 1990) :test #'=)

(alexandria:define-constant +new-atime-2+ (encode-universal-time 0 0 10 22 10 1999) :test #'=)

(alexandria:define-constant +new-mtime+   (encode-universal-time 0 0 11 23 11 1991) :test #'=)

(alexandria:define-constant +new-mtime-2+ (encode-universal-time 0 0 11 23 11 2001) :test #'=)

(defun example-change-access-time-file (path time &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (change-access-time stream root-fid path time)
      (let ((info (path-info stream root-fid path)))
        (stat-atime info)))))

(defun example-change-modify-time-file (path time &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (change-modify-time stream root-fid path time)
      (let ((info (path-info stream root-fid path)))
        (stat-mtime info)))))

(defun example-change-times-file (path atime mtime &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (change-time-values stream root-fid path atime mtime)
      (let ((info (path-info stream root-fid path)))
        (values (stat-atime info)
                (stat-mtime info))))))

(deftest test-change-access-time ((protocol-suite) (test-move-file))
  (assert-equality #'=
      +new-atime+
      (example-change-access-time-file (renamed-filename) +new-atime+))
  (assert-equality #'=
      +new-mtime+
      (example-change-modify-time-file (renamed-filename) +new-mtime+))
  (let ((expected-times (list +new-atime-2+ +new-mtime-2+)))
    (assert-equalp expected-times
        (multiple-value-list (example-change-times-file (renamed-filename)
                                                        +new-atime-2+
                                                        +new-mtime-2+)))))

(alexandria:define-constant +many-files-number+ 10000               :test #'=)

(alexandria:define-constant +many-files-path+   "/many/open/files/" :test #'string=)

(alexandria:define-constant +many-files-format+ "~a/~a.dummy"       :test #'string=)

(defun example-create-many-files (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (length (loop for i from 0 below +many-files-number+
                    collect
                    (let* ((cloned-fid  (clone-fid stream root-fid))
                           (created-fid (create-path stream
                                                     cloned-fid
                                                     (format nil
                                                             +many-files-format+
                                                             path
                                                             i))))
                      (9p-clunk stream created-fid)
                      cloned-fid))))))

(deftest test-create-many-files ((protocol-suite) (test-move-file))
  (assert-equality #'=
      +many-files-number+
      (ignore-errors (example-create-many-files +many-files-path+))))

(defun example-open-many-files (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (loop for i from 0 below 509 do
        (9p-clunk stream (open-path stream
                                    root-fid
                                    (format nil
                                            +many-files-format+
                                            path
                                            i))))
      t)))

(deftest test-open-many-files ((protocol-suite) (test-create-many-files))
  (assert-true (ignore-errors (example-open-many-files +many-files-path+))))

(defun example-directory-tree (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (collect-tree stream root-fid path))))

(deftest test-collect-dir-tree ((protocol-suite) (test-read))
  (assert-true (example-directory-tree "/dir")))

(alexandria:define-constant +new-mode+ #o650 :test #'=)

(defun example-change-mode (path new-mode &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (change-mode stream root-fid path new-mode)
      (let ((info (path-info stream root-fid path)))
        (logand (permissions-original-value (stat-mode info))
                 #x7ff)))))

(deftest test-change-access-time ((protocol-suite) (test-move-file))
  (assert-equality #'=
      +new-mode+
      (example-change-mode (renamed-filename) +new-mode+)))

(defun example-collect-children-many-times (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (root-fid        (mount stream root)))
      (loop repeat 10000 do
        (collect-directory-children stream root-fid path)))))

(defun example-open-path-many-times (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (purgatory::*fid* 0)
           (root-fid        (mount stream root)))
      (loop for i from 0 below 10000 do
        (9p-clunk stream (open-path stream root-fid path)))
      t)))

(deftest test-open-path-many-times ((protocol-suite) (test-write))
  (assert-true (ignore-errors (example-open-path-many-times *remote-test-path-write*))))

(defun example-collect-tree-many-times (path &optional (root "/"))
  (with-open-ssl-stream (stream
                         socket
                         *host*
                         *port*
                         *client-certificate*
                         *certificate-key*)
    (let* ((*messages-sent* ())
           (purgatory::*fid* 0)
           (root-fid        (mount stream root)))
      (loop for i from 0 below 10 do
        (format t "~a~%" i)
        (collect-tree stream root-fid path))
      t)))

(deftest test-collect-tree-many-times ((protocol-suite) (test-collect-dir-tree))
  (assert-true (ignore-errors (example-collect-tree-many-times "/"))))
